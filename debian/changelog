iwidgets4 (4.1.1-2) unstable; urgency=medium

  * Use Itcl/Itk 4 if they are already loaded into the Tcl interpreter,
    but load Itcl/Itk 3 by default. This helps with the transitional period
    with both Itcl/Itk 3 and 4 in use.
  * Add the VCS salsa headers.
  * Bump standards version to 4.1.4.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 21 Apr 2018 22:20:12 +0300

iwidgets4 (4.1.1-1) experimental; urgency=medium

  * New upstream release.
  * Fix the debian/watch uscan file.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 24 Mar 2018 14:40:12 +0300

iwidgets4 (4.1.0-2) experimental; urgency=medium

  * Switch to version 4 of itcl and itk.
  * Add a patch which relaxes the required Itcl version to just major version
    instead of the full patchlevel.
  * Bump standards version to 4.1.3.

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 07 Mar 2018 13:07:29 +0300

iwidgets4 (4.1.0-1) unstable; urgency=low

  * New upstream release.
  * Bump debhelper compatibility version to 10.
  * Bump standards version to 4.1.1.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 28 Oct 2017 10:11:27 +0300

iwidgets4 (4.0.1-6) unstable; urgency=low

  * Removed Chris Waters from the uploaders list (closes: #664110).
  * Added get-orig-source target to debian/rules.
  * Added ${misc:Depends} substitution variable to debian/control because
    the package uses debhelper.
  * Fixed errors in notebook, tabnotebook and tabset manpages.
  * Switched to 3.0 (quilt) source package format.
  * Bumped debhelper compatibility version to 8.
  * Bumped standards version to 3.9.3.

 -- Sergei Golovan <sgolovan@debian.org>  Wed, 30 May 2012 08:54:29 +0400

iwidgets4 (4.0.1-5) unstable; urgency=low

  * Switched to quilt for patch management.
  * Added uscan control file debian/watch.

 -- Sergei Golovan <sgolovan@debian.org>  Mon, 09 Jun 2008 21:27:11 +0400

iwidgets4 (4.0.1-4) unstable; urgency=low

  * New maintainer Debian Tcl/Tk Packagers
    <pkg-tcltk-devel@lists.alioth.debian.org>.
  * Added homepage header to debian/control.
  * Bumped standards version to 3.7.3.
  * Bumped debhelper compatibility level to 5.
  * Fixed ignoring make errors in clean target of debian/rules.
  * Debhelper is used in clean target, so moved it from build-depends-indep
    to build-depends.
  * Added build-dependency on tk-dev (just to please configure script,
    actually, info from tkConfig.sh and tclConfig.sh isn't used at all).
  * Moved Tcl library files to a subdirectory of /usr/share/tcltk to make the
    package compliant to Debian Tcl/Tk policy.
  * Moved demos to iwidgets4-doc binary package.
  * Restored forgotten demos images (closes: #404392).
  * Fixed brackets in manpage for scopedobject.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 02 Feb 2008 13:50:50 +0300

iwidgets4 (4.0.1-3) unstable; urgency=low

  * Moved "examples" link from /usr/share/doc to /usr/share/doc/iwidgets4
    where it was supposed to be in the first place (closes: #215117).

 -- Chris Waters <xtifr@debian.org>  Fri, 10 Oct 2003 10:28:00 -0700

iwidgets4 (4.0.1-2) unstable; urgency=low

  * Fixed build-dependencies to use tcl/tk/itcl/itk -dev packages.
  * Made debian/rules do chmod +x mkinstalldirs, since dpkg-source isn't
    actually smart enough to do that on its own (closes: #212308).

 -- Chris Waters <xtifr@debian.org>  Wed,  8 Oct 2003 12:28:39 -0700

iwidgets4 (4.0.1-1) unstable; urgency=low

  * Added "examples" link to demos in /usr/share/doc/iwidgets4.
  * First actual release to Debian archives.

 -- Chris Waters <xtifr@debian.org>  Sun, 24 Aug 2003 15:10:07 -0700

iwidgets4 (4.0.1-0pre2) unstable; urgency=low

  * Updated iwidgets testing package, still prerelease.  Changed package
    name.

 -- Chris Waters <xtifr@debian.org>  Tue, 19 Aug 2003 02:25:05 -0700

iwidgets4.0 (4.0.1-0pre1) unstable; urgency=low

  * New iwidgets package, prerelease testing package.

 -- Chris Waters <xtifr@debian.org>  Sat,  9 Aug 2003 10:55:06 -0700
